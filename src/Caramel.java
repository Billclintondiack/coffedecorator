
public class Caramel extends DecorateurIngredient {

	public Caramel(Boisson boisson) {
		super(boisson);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return boisson.getDescription()+ ", caramel";
	}

	@Override
	public double count() {
		// TODO Auto-generated method stub
		return 150.0 +boisson.count();
	}

}
