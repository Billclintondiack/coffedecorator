
public class Chocolat  extends DecorateurIngredient{

	public Chocolat(Boisson boisson) {
		super(boisson);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return boisson.getDescription()+ ", chocolat";
	}

	@Override
	public double count() {
		// TODO Auto-generated method stub
		return 100.0 +boisson.count();
	}

}
